<?php namespace Omnipay\Litle\Message\CreditCard;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Void Request
 */
class VoidRequest extends AbstractRequest
{

    /**
     * Get the raw data array for this message. The format of this varies from gateway to
     * gateway, but will usually be either an associative array, or a SimpleXMLElement.
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('transactionId');

        $data = $this->getBaseData();

        // Set Data
        $data['litleTxnId'] = $data['transactionId'];
        unset($data['orderSource']);
        unset($data['orderId']);

        return $data;
    }

    /**
     * Send the request with specified data
     *
     * @param  mixed $data The data to send
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->creditRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}