<?php namespace Omnipay\Litle\Message\CreditCard;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Authorize Request
 */
class AuthorizeRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('amount', 'card');
        $this->getCard()->validate();

        $data = $this->getBaseData();

        // Clean Amount
        $data['amount'] = $this->cleanAmount($data['amount']);

        // Retrieve Clean Billing
        $billToAddress = $this->cleanBillToAddress($data['card']);
        if (!empty($billToAddress)) {
            $data['billToAddress'] = $billToAddress;
        }

        // Clean up card
        $data['card'] = $this->cleanCard($data['card']);

        // If custom billing descriptor
        $customBillingDescriptor = $this->getParameter('billingDescriptor');
        if ($customBillingDescriptor) {
            $data['customBilling'] = array();
            $data['customBilling']['descriptor'] = $this->cleanBillingDescriptor($customBillingDescriptor);
        }

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->authorizationRequest($this->getData());

        return $this->response = new Response($this, $response);
    }

}