<?php namespace Omnipay\Litle\Message\CreditCard;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Create Token Request
 */
class CreateCardRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('orderId', 'id', 'accountNumber');

        $data = $this->getBaseData();

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->registerTokenRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}