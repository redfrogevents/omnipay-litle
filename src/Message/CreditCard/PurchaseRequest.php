<?php namespace Omnipay\Litle\Message\CreditCard;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Purchase Request
 */
class PurchaseRequest extends AuthorizeRequest
{

    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $data = parent::getData();

        // Set recycling request to none - REALLY IMPORTANT!!!
        // This ensures that this sale request doesnt attempt to make an attempt at a later time
        $data['recyclingRequest'] = array();
        $data['recyclingRequest']['recycleBy'] = 'None';

        // If custom billing descriptor
        $customBillingDescriptor = $this->getParameter('billingDescriptor');
        if ($customBillingDescriptor) {
            $data['customBilling'] = array();
            $data['customBilling']['descriptor'] = $this->cleanBillingDescriptor($customBillingDescriptor);
        }

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->saleRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}