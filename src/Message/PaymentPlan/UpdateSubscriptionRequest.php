<?php namespace Omnipay\Litle\Message\PaymentPlan;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Common\CreditCard;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;
use Omnipay\Litle\Message\Token\Token;

/**
 * Litle Create Subscription With Credit Card Request
 */
class UpdateSubscriptionRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('subscriptionId', 'card');

        $data = $this->getBaseData();

        // remove id
        unset($data['id']);

        // Set subscription id
        $data['subscriptionId'] = $this->getParameter('subscriptionId');

        // Credit card subscription
        if ($data['card'] instanceof CreditCard) {
            $this->getCard()->validate();

            // Clean up card
            $data['card'] = $this->cleanCard($data['card']);
        }

        // Token subscription
        if ($data['card'] instanceof Token) {
            // Convert card to mpos for litle request
            $data['token'] = array(
                'litleToken' => $data['card']->getLitleToken(),
                'expDate' => $data['card']->getExpiryDate('my'),
                'cardValidationNum' => $data['card']->getCvv()
            );
            unset($data['card']);
        }

        // var_dump($data);

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->updateSubscription($this->getData());

        return $this->response = new Response($this, $response);
    }
}