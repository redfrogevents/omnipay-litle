<?php namespace Omnipay\Litle\Message\PaymentPlan;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Create Payment Plan Request
 */
class CreatePlanRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('planCode', 'intervalType', 'numberOfPayments', 'amount', 'name', 'description');

        $data = $this->getBaseData();

        // Clean Amount
        $data['amount'] = $this->cleanAmount($data['amount']);

        // remove values not needed
        unset($data['id']);

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->createPlan($this->getData());

        return $this->response = new Response($this, $response);
    }
}