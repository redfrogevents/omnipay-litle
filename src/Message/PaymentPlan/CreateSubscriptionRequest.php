<?php namespace Omnipay\Litle\Message\PaymentPlan;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Common\CreditCard;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;
use Omnipay\Litle\Message\Token\Token;

/**
 * Litle Create Subscription With Credit Card Request
 */
class CreateSubscriptionRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('orderId', 'planCode', 'card');

        $data = $this->getBaseData();

        // Set amount to 0 - we dont want to charge them just assign them to a playment plan
        $data['amount'] = '0';

        // Set recuring request
        $data['recurringRequest'] = array(
            'subscription' => array()
        );

        // Set Plan code
        $data['recurringRequest']['subscription']['planCode'] = $this->getParameter('planCode');

        // Clean Amount
        if ($this->getParameter('amount') != '') {
            $data['recurringRequest']['subscription']['amount'] = $this->cleanAmount($this->getParameter('amount'));
        }

        // Set number of payments
        if ($this->getParameter('numberOfPayments') != '') {
            $data['recurringRequest']['subscription']['numberOfPayments'] = $this->getParameter('numberOfPayments');
        }

        // Set Start Date
        if ($this->getParameter('startDate') != '') {
            $data['recurringRequest']['subscription']['startDate'] = $this->getParameter('startDate');
        }

        // Credit card subscription
        if ($data['card'] instanceof CreditCard) {
            $this->getCard()->validate();

            // Retrieve Clean Billing
            $billToAddress = $this->cleanBillToAddress($data['card']);
            if (!empty($billToAddress)) {
                $data['billToAddress'] = $billToAddress;
            }

            // Clean up card
            $data['card'] = $this->cleanCard($data['card']);
        }

        // Token subscription
        if ($data['card'] instanceof Token) {
            // Convert card to mpos for litle request
            $data['token'] = array(
                'litleToken' => $data['card']->getLitleToken(),
                'expDate' => $data['card']->getExpiryDate('my'),
                'cardValidationNum' => $data['card']->getCvv()
            );
            unset($data['card']);
        }

        // If custom billing descriptor
        $customBillingDescriptor = $this->getParameter('billingDescriptor');
        if ($customBillingDescriptor) {
            $data['customBilling'] = array();
            $data['customBilling']['descriptor'] = $this->cleanBillingDescriptor($customBillingDescriptor);
        }

        // var_dump($data);

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->authorizationRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}