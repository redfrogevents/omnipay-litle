<?php namespace Omnipay\Litle\Message\PaymentPlan;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\AbstractRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Cancel Subscription
 */
class CancelSubscriptionRequest extends AbstractRequest
{
    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('subscriptionId');

        $data = $this->getBaseData();

        // Dont need id
        unset($data['id']);

        // Set subscription id
        $data['subscriptionId'] = $this->getParameter('subscriptionId');

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {

        $response = (new LitleOnlineRequest())->cancelSubscription($this->getData());

        return $this->response = new Response($this, $response);
    }
}