<?php namespace Omnipay\Litle\Message;

use Omnipay\Common\CreditCard;
use Omnipay\Litle\GettersSettersTraits;

/**
 * Litle Abstract Request
 */
abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{
    use GettersSettersTraits; // Parameters Getters and Setters

    protected $liveEndpoint = 'https://payments.litle.com/vap/communicator/online';
    protected $developerEndpoint = 'https://prelive.litle.com/vap/communicator/online';
    protected $baseData = array(
        'print_xml' => false,
        'timeout' => 65,
        'version' => '8.13',
        'proxy' => '',
        'litle_requests_path' => '',
        'batch_requests_path' => '',
        'batch_url' => '',
        'sftp_username' => '',
        'sftp_password' => '',
        'tcp_port' => '',
        'tcp_ssl' => '',
        'tcp_timeout' => ''
    );
    protected $cardTypes = array(
        'mastercard' => 'MC', // Mastercard
        'visa' => 'VI', // Visa
        'discover' => 'DI', // Discover
        'diners_club' => 'DC', // Diner Club
        'amex' => 'AX', // American Express
        'jcb' => 'JC' // Japanese Credit Bureau
    );

    /**
     * Override setCard to not set our card value
     * to new CreditCard if its not one already
     *
     * @param $value
     * @return $this|\Omnipay\Common\Message\AbstractRequest
     */
    public function setCard($value)
    {
        if ($value && is_array($value)) {
            $value = new CreditCard($value);
        }

        return $this->setParameter('card', $value);
    }

    /**
     * Set url based upon if in test mode
     *
     * @return string
     */
    public function getEndpoint()
    {
        return ($this->getTestMode() ? $this->developerEndpoint : $this->liveEndpoint);
    }

    /**
     * Take url, username, password and merchantId and merge base data
     *
     * @return array
     */
    protected function getBaseData()
    {
        $data = array_merge($this->getParameters(), $this->baseData);

        // Set url
        if ($data['url'] == '') {
            $data['url'] = $this->getEndpoint();
        }

        // Set username - litle wants user instead of username
        $data['user'] = $data['username'];
        unset($data['username']);

        return $data;
    }

    /**
     * Take a variation of different amount values and return
     * a cleaned version of what litle would like formated
     *
     * @param $amount
     * @return mixed|string
     */
    protected function cleanAmount($amount)
    {
        $amount = str_replace('$', '', $amount);
        $amount = str_replace(',', '', $amount);
        $amount = number_format($amount, 2, '.', '');
        $amount = str_replace('.', '', $amount);

        return $amount;
    }

    /**
     * Take in Omnipay type of credit card layout in either array or object(Ominipay/Common/CreditCard)
     * and return the formatted way litle would like to see its requests
     *
     * @param $card
     * @return array|CreditCard
     */
    protected function cleanCard($card)
    {
        // return card the way litle wants it
        $card = array(
            'number' => $card->getNumber(),
            'expDate' => $card->getExpiryDate('my'), // Ex: '0112',
            'cardValidationNum' => $card->getCvv(),
            'type' => $this->getAbbreviation($card->getBrand()) // 'MC'
        );

        return $card;
    }

    /**
     * Take in Omnipay type of credit card that contains billing information and return the formatted
     * way litle would like to recieve billing information
     *
     * @param $card
     * @return array
     */
    protected function cleanBillToAddress($card)
    {
        // return billing the way litle wants it
        $billing = array(
            'name' => $card->getBillingName(),
            'addressLine1' => $card->getBillingAddress1(),
            'city' => $card->getBillingCity(),
            'state' => $card->getBillingState(),
            'zip' => $card->getBillingPostcode(),
            'country' => $card->getBillingCountry()
        );

        // Check if bill to address actually contains info, if it doesn't return empty billing
        $isEmpty = true;
        foreach ($billing as $key => $value) {
            if (trim($value) != '') {
                $isEmpty = false;
            }
        }

        return ($isEmpty ? array() : $billing);
    }

    /**
     * Take billing descriptor and clean it so it acceptable to litle
     *
     * @param $str
     * @return string
     */
    protected function cleanBillingDescriptor($str)
    {
        $length = 25;
        $str = strtoupper($str); /* uppercase it */
        $str = str_replace(' ', '', $str); /* remove spaces */
        $str = preg_replace('/[^A-Za-z0-9\-*]/', '', $str); /* remove special chars */
        $str = substr($str, 0, $length); /* backup length filter */

        return $str;
    }

    /**
     * Take the Omnipay card type and retrieve the abbreviated version
     *
     * @param $cardType
     * @return string
     */
    protected
    function getAbbreviation(
        $cardType
    ) {
        foreach ($this->cardTypes as $cardTypeValue => $cardAbv) {
            if ($cardType == $cardTypeValue) {
                return $cardAbv;
            }
        }

        return '';
    }
}