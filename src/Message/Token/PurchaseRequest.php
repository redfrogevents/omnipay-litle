<?php namespace Omnipay\Litle\Message\Token;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Token Purchase Request
 */
class PurchaseRequest extends AbstractRequest
{

    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('amount', 'card');

        $data = $this->getBaseData();

        // Set recycling request to none - REALLY IMPORTANT!!!
        // This ensures that this sale request doesnt attempt to make an attempt at a later time
        $data['recyclingRequest'] = array();
        $data['recyclingRequest']['recycleBy'] = 'None';

        // Clean Amount
        $data['amount'] = $this->cleanAmount($data['amount']);

        // Convert card to mpos for litle request
        $data['token'] = $this->cleanToken($data['card']);
        unset($data['card']);

        // If custom billing descriptor
        $customBillingDescriptor = $this->getParameter('billingDescriptor');
        if ($customBillingDescriptor) {
            $data['customBilling'] = array();
            $data['customBilling']['descriptor'] = $this->cleanBillingDescriptor($customBillingDescriptor);
        }

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->saleRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}