<?php namespace Omnipay\Litle\Message\Token;

/**
 * Litle Token Abstract Request
 */
abstract class AbstractRequest extends \Omnipay\Litle\Message\AbstractRequest
{
    protected function cleanToken(Token $token) {
        return array(
            'litleToken' => $token->getLitleToken(),
            'expDate' => $token->getExpiryDate('my'),
            'cardValidationNum' => $token->getCvv()
        );
    }
}