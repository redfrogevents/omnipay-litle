<?php namespace Omnipay\Litle\Message;

use litle\sdk\XmlParser;
use Omnipay\Common\Message\AbstractResponse;

/**
 * Litle Response
 */
class Response extends AbstractResponse
{

    protected $request;
    protected $data;
    protected $message;

    /**
     * Determine whether or not based upon the response if the
     * request was retruned as successful or not.
     *
     * @return bool
     */
    public function isSuccessful()
    {
        return (in_array($this->getResponseNumber(), ['000', '801', '802']));
    }

    /**
     * Get the return data from litle
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the response number
     *
     * @return mixed
     */
    public function getResponseNumber()
    {
        return XmlParser::getNode($this->data, 'response');
    }

    /**
     * Return the transaction id from litle
     *
     * @return string
     */
    public function getTransactionReference()
    {
        if ($this->isSuccessful()) {
            return XMLParser::getNode($this->data, 'litleTxnId');
        }

        return '';
    }

    /**
     * Return the card reference number(token id) from litle
     *
     * @return string
     */
    public function getCardReference()
    {
        return XmlParser::getNode($this->data, 'litleToken');
    }

    /**
     * If signed up for a subscription get subscription id
     *
     * @return string
     */
    public function getSubscriptionId()
    {
        return XmlParser::getNode($this->data, 'subscriptionId');
    }

    /**
     * Get errors if not successful or get message from litle response
     * Most likely if successful litle will return "Approved"
     *
     * @return array|string
     */
    public function getMessage()
    {
        // Check if format response is valid
        $validFormatResponse = XmlParser::getAttribute($this->data, 'litleOnlineResponse', 'response');
        if ($validFormatResponse == 1) { // 1 == not valid
            // Take message and add to errors
            $validFormatMessage = XmlParser::getAttribute($this->data, 'litleOnlineResponse', 'message');

            return $validFormatMessage;
        }

        // If litleOnlineResponse was successful return normal message
        return XMLParser::getNode($this->data, 'message');
    }
}
