<?php namespace Omnipay\Litle\Message\Mobile;

use Omnipay\Common\Helper;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Mobile Payment class
 */
class Mobile implements MobileInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected $parameters;

    /**
     * Create a new Mobile object using the specified parameters
     *
     * @param array $parameters An array of parameters to set on the new object
     */
    public function __construct($parameters = null)
    {
        $this->initialize($parameters);
    }

    /**
     * Initialize the object with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters
     *
     * @return $this
     */
    public function initialize($parameters = null)
    {
        $this->parameters = new ParameterBag;

        Helper::initialize($this, $parameters);

        return $this;
    }

    public function getParameters()
    {
        return $this->parameters->all();
    }

    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    protected function setParameter($key, $value)
    {
        $this->parameters->set($key, $value);

        return $this;
    }

    public function getKsn()
    {
        return $this->getParameter('ksn');
    }

    public function setKsn($value)
    {
        return $this->setParameter('ksn', $value);
    }

    public function getFormatId()
    {
        return $this->getParameter('formatId');
    }

    public function setFormatId($value)
    {
        return $this->setParameter('formatId', $value);
    }

    public function getEncryptedTrack()
    {
        return $this->getParameter('encryptedTrack');
    }

    public function setEncryptedTrack($value)
    {
        return $this->setParameter('encryptedTrack', $value);
    }

    public function getTrack1Status()
    {
        return $this->getParameter('track1Status');
    }

    public function setTrack1Status($value)
    {
        return $this->setParameter('track1Status', $value);
    }

    public function getTrack2Status()
    {
        return $this->getParameter('track2Status');
    }

    public function setTrack2Status($value)
    {
        return $this->setParameter('track2Status', $value);
    }

}