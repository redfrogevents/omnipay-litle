<?php namespace Omnipay\Litle\Message\Mobile;

/**
 * Litle Mobile Abstract Request
 */
abstract class AbstractRequest extends \Omnipay\Litle\Message\AbstractRequest
{
    protected function cleanMpos(Mobile $mobile) {
        return array(
            'ksn' => $mobile->getKsn(),
            'formatId' => $mobile->getFormatId(),
            'encryptedTrack' => $mobile->getEncryptedTrack(),
            'track1Status' => $mobile->getTrack1Status(),
            'track2Status' => $mobile->getTrack2Status()
        );
    }
}