<?php namespace Omnipay\Litle\Message\Mobile;

use litle\sdk\LitleOnlineRequest;
use Omnipay\Litle\Message\Response;

/**
 * Litle Mobile Purchase Request
 */
class PurchaseRequest extends AbstractRequest
{

    /**
     * Get baseData and make updates to align with litle requests
     *
     * @return mixed
     */
    public function getData()
    {
        $this->validate('amount', 'card');

        $data = $this->getBaseData();

        // Override orderSource for Mobile purchase - litle required this
        $data['orderSource'] = 'retail';

        // Clean Amount
        $data['amount'] = $this->cleanAmount($data['amount']);

        // Convert card to mpos for litle request
        $data['mpos'] = $this->cleanMpos($data['card']);
        unset($data['card']);

        // If custom billing descriptor
        $customBillingDescriptor = $this->getParameter('billingDescriptor');
        if ($customBillingDescriptor) {
            $data['customBilling'] = array();
            $data['customBilling']['descriptor'] = $this->cleanBillingDescriptor($customBillingDescriptor);
        }

        return $data;
    }

    /**
     * Override Omnipay's use of using http client to send request
     * and use the litle sdk instead.
     *
     * @param mixed $data
     * @return Response
     */
    public function sendData($data)
    {
        $response = (new LitleOnlineRequest())->saleRequest($this->getData());

        return $this->response = new Response($this, $response);
    }
}