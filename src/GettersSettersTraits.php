<?php namespace Omnipay\Litle;

trait GettersSettersTraits
{
    public function getUrl()
    {
        return $this->getParameter('url');
    }

    public function setUrl($value)
    {
        return $this->setParameter('url', $value);
    }

    public function getUsername()
    {
        return $this->getParameter('username');
    }

    public function setUsername($value)
    {
        return $this->setParameter('username', $value);
    }

    public function getPassword()
    {
        return $this->getParameter('password');
    }

    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getOrderId()
    {
        return $this->getParameter('orderId');
    }

    public function setOrderId($value)
    {
        return $this->setParameter('orderId', $value);
    }

    public function getOrderSource()
    {
        return $this->getParameter('orderSource');
    }

    public function setOrderSource($value)
    {
        return $this->setParameter('orderSource', $value);
    }

    public function getReportGroup()
    {
        return $this->getParameter('reportGroup');
    }

    public function setReportGroup($value)
    {
        return $this->setParameter('reportGroup', $value);
    }

    public function getId()
    {
        return $this->getParameter('id');
    }

    public function setId($value)
    {
        return $this->setParameter('id', $value);
    }

    public function getAccountNumber()
    {
        return $this->getParameter('accountNumber');
    }

    public function setAccountNumber($value)
    {
        return $this->setParameter('accountNumber', $value);
    }

    // Billing Descriptor
    public function getBillingDescriptor()
    {
        return $this->getParameter('billingDescriptor');
    }

    public function setBillingDescriptor($value)
    {
        return $this->setParameter('billingDescriptor', $value);
    }


    // Payment plan specific methods
    public function getPlanCode()
    {
        return $this->getParameter('planCode');
    }

    public function setPlanCode($value)
    {
        return $this->setParameter('planCode', $value);
    }

    public function getIntervalType()
    {
        // Return interval type in caps. Vantiv requires it in caps.
        return strtoupper($this->getParameter('intervalType'));
    }

    public function setIntervalType($value)
    {
        // Vantiv requires it in caps
        return $this->setParameter('intervalType', strtoupper($value));
    }

    public function getNumberOfPayments()
    {
        return $this->getParamter('numberOfPayments');
    }

    public function setNumberOfPayments($value)
    {
        return $this->setParameter('numberOfPayments', $value);
    }

    public function getName()
    {
        return $this->getParamter('name');
    }

    public function setName($value)
    {
        return $this->setParameter('name', $value);
    }

    public function getDescription()
    {
        return $this->getParamter('description');
    }

    public function setDescription($value)
    {
        return $this->setParameter('description', $value);
    }

    public function getStartDate()
    {
        if ($this->getParamter('startDate') != '') {
            return date('Y-m-d', strtotime($this->getParamter('startDate')));
        }

        return '';
    }

    public function setStartDate($value)
    {
        return $this->setParameter('startDate', date('Y-m-d', strtotime($value)));
    }

    public function getSubscriptionId()
    {
        return $this->getParameter('subscriptionId');
    }

    public function setSubscriptionId($value)
    {
        return $this->setParameter('subscriptionId', $value);
    }
}