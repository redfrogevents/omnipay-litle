<?php namespace Omnipay\Litle;

use Omnipay\Common\AbstractGateway;
use Omnipay\Common\CreditCard;
use Omnipay\Litle\GettersSettersTraits;
use Omnipay\Litle\Message\Mobile\Mobile;
use Omnipay\Litle\Message\Token\Token;

/**
 * Litle Class
 */
class Gateway extends AbstractGateway
{
    use GettersSettersTraits;

    public function getName()
    {
        return 'Litle';
    }

    public function getDefaultParameters()
    {
        return array(
            'url' => '',
            'username' => '',
            'password' => '',
            'merchantId' => '',
            'orderId' => '',
            'testMode' => false,
            'orderSource' => 'ecommerce',
            'reportGroup' => 'Default',
            'id' => '',
            'accountNumber' => ''
        );
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\CreditCard\AuthorizeRequest
     */
    public function authorize(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\CreditCard\AuthorizeRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\CreditCard\PurchaseRequest
     */
    public function purchase(array $parameters = array())
    {
        if (isset($parameters['card'])) {
            // Identify if purchase type is array or object if array convert to Omnipay\Common\CreditCard
            if (is_array($parameters['card'])) {
                $parameters['card'] = new CreditCard($parameters['card']);
            }

            // Identify purchase type
            if ($parameters['card'] instanceof Mobile) {
                return $this->createRequest('\Omnipay\Litle\Message\Mobile\PurchaseRequest', $parameters);
            } elseif ($parameters['card'] instanceof Token) {
                return $this->createRequest('\Omnipay\Litle\Message\Token\PurchaseRequest', $parameters);
            }
        }

        // Default to credit card purchase request
        return $this->createRequest('\Omnipay\Litle\Message\CreditCard\PurchaseRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\CreditCard\RefundRequest
     */
    public function refund(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\CreditCard\RefundRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\CreditCard\RefundRequest
     */
    public function void(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\CreditCard\VoidRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\CreditCard\CreateCardRequest
     */
    public function createCard(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\CreditCard\CreateCardRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\PaymentPlan\CreatePlanRequest
     */
    public function createPlan(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\PaymentPlan\CreatePlanRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\PaymentPlan\CreateSubscriptionRequest
     */
    public function createSubscription(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\PaymentPlan\CreateSubscriptionRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\PaymentPlan\CreateSubscriptionRequest
     */
    public function updateSubscription(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\PaymentPlan\UpdateSubscriptionRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return \Omnipay\Litle\Message\PaymentPlan\CancelSubscriptionRequest
     */
    public function cancelSubscription(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Litle\Message\PaymentPlan\CancelSubscriptionRequest', $parameters);
    }
}