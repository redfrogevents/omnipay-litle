# Omnipay: Litle

**Litle driver for the Omnipay PHP payment processing library**

[Omnipay](https://github.com/thephpleague/omnipay) is a framework agnostic, multi-gateway payment
processing library for PHP 5.3+. This package implements Litle support for Omnipay.

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, simply add it
to your `composer.json` file:

```json
{
    "require": {
        "omnipay/litle": "~2.*"
    }
}
```

And run composer to update your dependencies:

    $ curl -s http://getcomposer.org/installer | php
    $ php composer.phar update
    
For testing create litle_SDK_config.ini file
    
    $ Rename litle_SDK_config.example.ini to litle_SDK_config.ini  
    $ Update config field with your credentials
    $ run phpunit


For general usage instructions, please see the main [Omnipay](https://github.com/thephpleague/omnipay)
repository.

## Usage

Reference testing for use case scenarios

## Methods

* Authorize
* Purchase
    > Normal
    > Mobile
* Refund
* Void

## Support

If you are having general issues with Omnipay, we suggest posting on
[Stack Overflow](http://stackoverflow.com/). Be sure to add the
[omnipay tag](http://stackoverflow.com/questions/tagged/omnipay) so it can be easily found.

If you want to keep up to date with release anouncements, discuss ideas for the project,
or ask more detailed questions, there is also a [mailing list](https://groups.google.com/forum/#!forum/omnipay) which
you can subscribe to.