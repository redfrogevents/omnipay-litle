<?php namespace Omnipay\Litle;

use Exception;
use Omnipay\Common\CreditCard;
use Omnipay\Litle\Message\Mobile\Mobile;
use Omnipay\Litle\Message\Token\Token;
use Omnipay\Tests\GatewayTestCase;

class GatewayTest extends GatewayTestCase
{
    public $gateway;
    public $testAuthorize;
    public $testPurchase;
    public $testRefund;
    public $testMobile;
    public $testCard;
    public $testCardPurchase;
    public $testPlan;
    public $testSubscription;
    public $testSubscriptionUpdate;
    public $testSubscriptionCancel;

    public function setUp()
    {
        parent::setUp();

        // Parse ini file
        $iniFile = "litle_SDK_config.ini";
        try {
            if (($litle_config = @parse_ini_file($iniFile, true)) == false) {
                throw new Exception('Missing file: ' . $iniFile . " Copy example file and update variables\n\n");
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }

        // Initiate new gateway
        $this->gateway = new Gateway();
        $this->gateway->setTestMode(true);
        $this->gateway->setUsername($litle_config['user']);
        $this->gateway->setPassword($litle_config['password']);
        $this->gateway->setMerchantId($litle_config['merchantId']);

        // Example litle sale fields
//        'orderId' => '1',
//        'amount'  => '10010',
//        'orderSource' => 'ecommerce',
//        'billToAddress' => array(
//              'name' => 'John Smith' ,
//              'addressLine1' => ' 1 Main St.',
//              'city' => 'Burlington' ,
//              'state' => 'MA' ,
//              'zip' => '0183-3747',
//              'country' => 'US'
//        ),
//        'card' => array(
//              'number' => '5112010000000003',
//              'expDate' => '0112',
//              'cardValidationNum' => '349',
//              'type' => 'MC'
//        )

        // Example litle create token fields
//        'orderId'=>'12344',
//        'id'=> '456',
//        'accountNumber'=>'1233456789103801'

        // Custom Billing
        $customBilling = 'test fd%$#@^#55 212fjdsijf disojfiodsjiof dsio';

        $this->testAuthorize = array(
            'id' => rand(5000, 100000000), // Optional - but litle requested
            'orderId' => rand(5000, 100000000), // Required - but not part of omnipay
            'amount' => '10.00',
            'billingDescriptor' => $customBilling,
            'card' => new CreditCard(
                array(
                    // Card
                    'name' => 'John Smith',
                    'number' => '4111111111111111',
                    'expiryMonth' => '12',
                    'expiryYear' => '2016',
                    'cvv' => '123',
                    // Billing
                    'billingAddress1' => '1 Main St.',
                    'billingAddress2' => '',
                    'billingCity' => 'Burlington',
                    'billingPostcode' => '',
                    'billingState' => 'MA',
                    'billingCountry' => 'US'
                )
            )
        );

        $this->testPurchase = array(
            'id' => rand(5000, 100000000), // Optional - but litle requested
            'orderId' => rand(5000, 100000000), // Required - but not part of omnipay
            'amount' => '10.00',
            'billingDescriptor' => $customBilling,
            'card' => new CreditCard(
                array(
                    // Card
                    'name' => 'John Smith',
                    'number' => '4111111111111111',
                    'expiryMonth' => '12',
                    'expiryYear' => '2016',
                    'cvv' => '123',
                    // Billing
                    'billingAddress1' => '1 Main St.',
                    'billingAddress2' => '',
                    'billingCity' => 'Burlington',
                    'billingPostcode' => '',
                    'billingState' => 'MA',
                    'billingCountry' => 'US'
                )
            )
        );

        $this->testRefund = array(
            'id' => rand(5000, 100000000), // Optional - but litle requested
            'orderId' => '', // Required - but not part of omnipay
            'transactionId' => '',
            'amount' => '10.00',
            'billingDescriptor' => $customBilling,
        );

        // ksn, formatId, encryptedTrack, track1Status, track2Status
        $this->testMobile = array(
            'id' => rand(5000, 100000000), // Optional - but litle requested
            'orderId' => rand(5000, 100000000), // Required - but not part of omnipay
            'amount' => '10.00',
            'billingDescriptor' => $customBilling,
            'card' => new Mobile(
                array(
                    'ksn' => '77853211300008E0 0016',
                    'formatId' => '30',
                    'encryptedTrack' => 'CASE1E185EADD6AFE78C9A214B21313DCD836FDD555FBE3A6C48D141FE80AB9172B9 63265AFF72111895FE415DEDA162CE8CB7AC4D91EDB611A2AB756AA9CB1A000000000 000000000000000000000005A7AAF5E8885A9DB88ECD2430C497003F2646619A2382FFF20 5767492306AC804E8E64E8EA6981DD',
                    'track1Status' => '0',
                    'track2Status' => '0'
                )
            )
        );

        $this->testCard = array(
            'id' => rand(5000, 100000000),
            'orderId' => rand(5000, 100000000),
            'accountNumber' => '4457119922390123'
        );

        $this->testCardPurchase = array(
            'id' => rand(5000, 100000000),
            'orderId' => rand(5000, 100000000),
            'amount' => '100.10',
            'card' => new Token(
                array(
                    'litleToken' => '',
                    'expiryMonth' => '01',
                    'expiryYear' => '18',
                    'cvv' => '123'
                )
            )
        );

        $this->testPlan = array(
            'planCode' => uniqid(),
            'intervalType' => 'monthly',
            'numberOfPayments' => 5,
            'amount' => '150.00',
            'name' => uniqid(),
            'description' => 'Description' . uniqid(),
        );

        $this->testSubscription = array(
            'orderId' => uniqid(),
            'billingDescriptor' => $customBilling,
            'planCode' => $this->testPlan['planCode'], // Plan code is from the test plan
            'startDate' => date('Y-m-d', strtotime('+1 month')),
            'amount' => '50.00',
            'numberOfPayments' => 10,
            'card' => $this->testPurchase['card'],
        );

        $this->testSubscriptionUpdate = array(
            // 'orderId' => $this->testSubscription['orderId'],
            'subscriptionId' => '',
            'card' => $this->testSubscription['card']
        );

        $this->testSubscriptionCancel = array(
            'subscriptionId' => ''
        );
    }

    public function testAuthorize()
    {
        $response = $this->gateway->authorize($this->testAuthorize)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testAuthorizeWithoutBilling()
    {
        $testAuthorized = $this->testAuthorize;
        unset($testAuthorized['billingAddress1']);
        unset($testAuthorized['billingAddress2']);
        unset($testAuthorized['billingCity']);
        unset($testAuthorized['billingPostcode']);
        unset($testAuthorized['billingState']);
        unset($testAuthorized['billingCountry']);

        $response = $this->gateway->authorize($testAuthorized)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testPurchase()
    {
        $response = $this->gateway->purchase($this->testPurchase)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');

        // Test refund
        $this->testRefund['transactionId'] = $response->getTransactionReference();

        $response = $this->gateway->refund($this->testRefund)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testPurchaseWithoutBilling()
    {
        $testPurchase = $this->testPurchase;
        unset($testPurchase['billingAddress1']);
        unset($testPurchase['billingAddress2']);
        unset($testPurchase['billingCity']);
        unset($testPurchase['billingPostcode']);
        unset($testPurchase['billingState']);
        unset($testPurchase['billingCountry']);

        $response = $this->gateway->purchase($testPurchase)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');

        // Test void
        $response = $this->gateway->void(array('transactionId' => $response->getTransactionReference()))->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testMobilePurchase()
    {
        $response = $this->gateway->purchase($this->testMobile)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testCreateCardAndSale()
    {
        $response = $this->gateway->createCard($this->testCard)->send();

        // $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Account number was previously registered');
        $this->assertNotEmpty($response->getCardReference());

        // Set token and make purchase with token
        $this->testCardPurchase['card']->setlitleToken($response->getCardReference());

        $response = $this->gateway->purchase($this->testCardPurchase)->send();

        // var_dump($response->getResponseNumber());

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
    }

    public function testCreatePlanAndSubscription()
    {
        // Create Payment Plan
        $response = $this->gateway->createPlan($this->testPlan)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');

        // var_dump($response->getMessage());

        // Create Subscription with credit card
        $response = $this->gateway->createSubscription($this->testSubscription)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
        $this->assertTrue($response->getSubscriptionId() != '');

        // Cancel Subscription - commented out because litle expects specific create subscription request
//        $this->testSubscriptionCancel['subscriptionId'] = $response->getSubscriptionId();
//        $response = $this->gateway->cancelSubscription($this->testSubscriptionCancel)->send();
//        var_dump($response->getMessage());
//        $this->assertTrue($response->isSuccessful());
//        $this->assertTrue($response->getMessage() == 'Approved');
//        $this->assertTrue($response->getSubscriptionId() != '');

        // var_dump($response->getMessage());

        // Create card and test subscription with card
        $response = $this->gateway->createCard($this->testCard)->send();

        // $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Account number was previously registered');
        $this->assertNotEmpty($response->getCardReference());

        // Set testSubscription to use token, then make purchase with token
        $this->testSubscription['card'] = $this->testCardPurchase['card'];
        $this->testSubscription['card']->setlitleToken($response->getCardReference());
        $response = $this->gateway->createSubscription($this->testSubscription)->send();
        $subscriptionId = $response->getSubscriptionId();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->getMessage() == 'Approved');
        $this->assertTrue($response->getSubscriptionId() != '');

        // Update subscription - commented out because litle expects specific create subscription request
//        $this->testSubscriptionUpdate['subscriptionId'] = $subscriptionId;
//        $response = $this->gateway->updateSubscription($this->testSubscriptionUpdate)->send();
//        $this->assertTrue($response->isSuccessful());
//        $this->assertTrue($response->getMessage() == 'Approved');

        // Cancel Subscription - commented out because litle expects specific create subscription request
//        $this->testSubscriptionCancel['subscriptionId'] = $subscriptionId;
//        $response = $this->gateway->cancelSubscription($this->testSubscriptionCancel)->send();
//        $this->assertTrue($response->isSuccessful());
//        $this->assertTrue($response->getMessage() == 'Approved');
//        $this->assertTrue($response->getSubscriptionId() != '');

        // var_dump($response->getSubscriptionId());
        // var_dump($response->getMessage());
    }

}